'''
Created on May 19, 2017

@author: Varela
'''
import numpy as np 
import  pandas as pd 


def get_data():
#     df = pd.read_csv('../src/projects/ecommerce/ecommerce_data.csv')
    df = pd.read_csv('ecommerce_data.csv')
    data = df.as_matrix()
    X = data[:,:-1]
    Y = data[:,-1]
    
    X[:,1] = (X[:,1] - X[:,1].mean()) / X[:,1].std()
    X[:,2] = (X[:,2] - X[:,2].mean()) / X[:,2].std()
    n,d = X.shape
    X2 = np.zeros((n,d+3))
    X2[:,0:(d-1)] = X[:,0:(d-1)]
    
    for i in xrange(n):
        t = int(X[i,d-1])
        X2[i,t+d-1]=1
    Z = np.zeros((n,4))
    Z[np.arange(n), X[:,d-1].astype(np.int32)] =1
    #X2[:,-4:] = Z
    assert(np.abs(X2[:,-4:] - Z).sum() < 10e-10)
    
    return X2, Y

def get_binary_data():
    X, Y = get_data()
    X2 = X[Y <= 1]
    Y2  = Y[Y <= 1]
    return X2, Y2

X,Y = get_binary_data()
print X[1,:]