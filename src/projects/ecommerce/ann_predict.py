'''
Created on May 19, 2017

@author: Varela
'''

import numpy as np 
from process import get_data
import util.util as utl 
# objective go forward on a neural network and make predictions

X, Y = get_data()
m =  5
d = X.shape[1]
k = len(set(Y))

W1 = np.random.randn(d, m)
b1 = np.random.randn(m)
W2 = np.random.randn(m, k)
b2 = np.zeros(k)

def softmax(a):
    expA =  np.exp(a)
    return expA / expA.sum(axis=1, keepdims=True)

def forward(X, W1, b1, W2, b2):
    Z = np.tanh(X.dot(W1) + b1)
    return softmax(Z.dot(W2) + b2)

P_Y_given_X = forward(X, W1, b1, W2, b2)
predictions = np.argmax(P_Y_given_X, axis=1)
def classification_rate(Y,P):
    return np.mean(Y ==P)

print "score tanh",    classification_rate(Y, predictions)
print "score sigmoid", classification_rate(Y, utl.fwdprop(X, W1, b1, W2, b2) )



